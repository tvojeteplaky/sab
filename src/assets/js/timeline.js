var Timeline = {
    timelines: [],
    top_inrementation: 10,
    top_inc_start: 24,
    min_dif: 40,

    init: function (timel) {
        Timeline.timelines.push(timel);
        Timeline.initYears(timel);
    },

    initYears: function (line) {
        var items = Timeline.get_items(line);

        var top_1, top_2;
        for (var i = 0; i < items.length; i++) {
            if (i > 0 && items[i].length == 1) {
                top_1 = items[i-1].position().top;
                top_2 = items[i].position().top;

                if ((top_2 - top_1) < Timeline.min_dif) {
                    items[i].css("margin-top", (Math.abs(top_2 - top_1) + Timeline.min_dif) + "px");
                }
            }
        }
    },

    get_prev_height: function(line, right, iter) {
        var height = 0;
        line.find((right) ? ".right" : ".left").find(".item").each(function(index){
            if (index < iter)  {
                height += $(this).find(".container").outerHeight(true) + parseInt($(this).css("margin-top"));
            }
        });

        return height;
    },

    get_items: function(line) {
        var items = [];

        line.find(".right").find(".item").each(function(index){
            items.push($(this));
            items.push(line.find(".left").find(".item:nth-of-type(" + (index + 1) + ")"));
        });

        return items;
    }
}
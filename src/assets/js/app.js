$(document).foundation();
Foundation.Abide.defaults.patterns['ico'] = /^[0-9]{8}$/;
Foundation.Abide.defaults.patterns['tel'] = /^[+]?[()\/0-9. -]{9,}$/;

$(window).load(function() {
	$("#loader").fadeOut("slow");
});
new WOW().init();

var optionsMil = {
  useEasing : true, 
  useGrouping : true, 
  separator : '', 
  decimal : '.', 
  prefix : '', 
  suffix : '&nbsp;mld.' 
};


var options = {
  useEasing : true, 
  useGrouping : true, 
  separator : '&nbsp;', 
  decimal : '.', 
  prefix : '', 
  suffix : '' 
};
if (!!document.getElementById("countMil")) {
	var mil = new CountUp("countMil", 0, document.getElementById("countMil").innerText.replace("mld.",""), 0, 3, optionsMil); 
	var first = new CountUp("countFirst", 0, document.getElementById("countFirst").innerText.replace(" ",""), 0, 3, options);
	var sec = new CountUp("countSec", 0, document.getElementById("countSec").innerText.replace(" ",""), 0, 3, options);
}
$(window).load(function(){
	if (!!document.getElementById("countMil")) {
		mil.start();
		first.start();
		sec.start();
	}
});

function slideTo(attr,wtop) {
	if($(attr).length>0){
		$('html, body').animate({
			scrollTop: ($(attr).offset().top - wtop)
		}, 2000);
		$("#sub_menu a").removeClass("actived");
		$("#sub_menu a[href='"+attr.replace("#","/")+"']").addClass("actived");
	}
}

function isScrolledIntoView(elem)
{
	var $elem = $(elem);
	var $window = $(window);

	var docViewTop = $window.scrollTop();
	var docViewBottom = docViewTop + $window.height();

	var elemTop = $elem.offset().top;
	var elemBottom = elemTop + $elem.height();
	
	return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
}


$(document).ready(function() {
	$('.banky').slick({
		infinite: true,
		prevArrow:'<button type="button" class="slick-prev"></button>',
		nextArrow:'<button type="button" class="slick-next"></button>',
		dots: false,
		slidesToShow: 4,
		slidesToScroll: 1,
	});

	$('#tym-slider').slick({
		prevArrow:'<button type="button" class="slick-prev"></button>',
		nextArrow:'<button type="button" class="slick-next"></button>',
		dots: false,
		infinite: true,

		slidesToShow: 4,
		slidesToScroll: 3,
		responsive: [
		{
	  		breakpoint: 1024,
	  		settings: {
				slidesToShow: 3,
				slidesToScroll: 3,
	  		}
		},
		{
	  		breakpoint: 600,
	  		settings: {
				slidesToShow: 2,
				slidesToScroll: 2
	  		}
		}]
	});
	if($(".timeline").length>0) {
		var top = 0;
		var step = 300;
		var max = $("#scrollTimeline").outerHeight() - 600;
		$("button.timeline-up").addClass("invisible");


		$("button.timeline-up").click(function(){
			$("#scrollTimeline").promise().done(function() {
				if(top>0) {
					top -= step;
					$("#scrollTimeline").animate({'top':'+='+step+'px'},600);
					$("button.timeline-down").removeClass("invisible");
				} else {
					$("button.timeline-up").addClass("invisible");
				}
			});

		});
		$("button.timeline-down").click(function(ev){
			$("#scrollTimeline").promise().done(function() {
				if (top==0 || top<=max) {
					top+=step;
					$("#scrollTimeline").animate({'top':'-='+step+'px'},600);
					$("button.timeline-up").removeClass("invisible");

				} else {
					$("button.timeline-down").addClass("invisible");
				}
			});
		});
	}

	if($("form").length>0) {
	
		/*$(".continue").click(function() {
			var form = $(this).closest("form");
			if(form.find(".form-step-1").hasClass("hide")) {
				form.find(".form-step-1").removeClass("hide");
				form.find(".continue").addClass("hide");
			} */
				
				
				$("form").find("#typKlienta").change(function() {
					var form = $(this).closest("form");
					$(form.find(".form-step-2")).removeClass("hide");
					if($("#typKlienta").val()=="Fyzická osoba nepodnikající") {
						$(form.find(".form-step-2-a")).removeClass("hide");
						$(form.find(".form-step-2-b")).addClass("hide");
						$('input,textarea,select').filter(':visible').attr("required");
						$('input,textarea,select').filter(':hidden').removeAttr("required");
						$(form.find("button[type='submit']").removeClass("hide"));
						$(form.find(".continue").addClass("hide"));
					} else {
						$(form.find(".form-step-2-a")).addClass("hide");
						$(form.find(".form-step-2-b")).removeClass("hide");
						$('input,textarea,select').filter(':visible').attr("required");
						$('input,textarea,select').filter(':hidden').removeAttr("required");
						$(form.find("button[type='submit']").removeClass("hide"));
						$(form.find(".continue").addClass("hide"));
					}
				});

			
		//});
		$("form").submit(function(ev) {
			ev.preventDefault();
			var data = $(this).serialize();
			var form = $(this);
			if (!($(".is-invalid-input").length>0 )) {
				$.post("?",data,function() {
						// $(".information,.info").removeClass("hide");
						if ($(form).hasClass("newsletter")) {
							$(form).find("[name=newsletter_email]").val("");
							$("#newsletterModal").foundation('open');
						} else {
							$(form).foundation('resetForm');
							window.location.replace($(form).find("[name='h_param']").val());
							//$("#contactModal").foundation('open');
						}
					});
			}
			
		});
	}
			
	if ($("#faq-show").length>0) {
		$("#faq-show").click(function(ev) {
			ev.preventDefault();
			
			$(".faq .invisible").addClass("animated slideInUp");
			$(".faq li").removeClass("hide");
			$(".faq li").removeClass("invisible");
			$(this).addClass("hide");
		}); 
	}
	if ($("#co-rikaji-button").length>0) {
		$("#co-rikaji-button").click(function(ev) {
			ev.preventDefault();
			$("#co_rikaji_backround div.invisible").addClass("animated slideInUp");
			$("#co_rikaji_backround div").removeClass("hide");
			$("#co_rikaji_backround div").removeClass("invisible");
			$(this).addClass("hide");
		}); 
	}
			
	if($("#sub_menu").length>0) {
		$("#sub_menu a").each(function(k,v) {
			if($($(v).attr("href").replace("/","#")).length>0) {
				$(v).click(function(e) {
					e.preventDefault();
					slideTo($(v).attr("href").replace("/","#"),172);

				});
			}
		});
	}
	if(window.location.hash != ""||window.location.hash!="#") {
		slideTo(window.location.hash,10);
	}
});

$(document).scroll(function() {
	if($("#sub_menu").length>0) {
		$($("#sub_menu a").toArray().reverse()).each(function(k,v) {
			if($($(v).attr("href").replace("/","#")).length>0 && isScrolledIntoView($(v).attr("href").replace("/","#"))){
				$("#sub_menu a").removeClass("actived");
				$(v).addClass("actived");
			}
		});
	}
});